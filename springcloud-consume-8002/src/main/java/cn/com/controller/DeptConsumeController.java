package cn.com.controller;


import cn.com.springcloudPojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class DeptConsumeController {

    @Autowired
    private RestTemplate restTemplate;

    //private static final String REST_URL_PREFIX = "http://localhost:8001";
    private static final String REST_URL_PREFIX = "http://SPRINGCLOUD-PROVIDER-DEPT-8001";

    @RequestMapping("/consume/list")
    public List<Dept> getList(){

        return restTemplate.getForObject(REST_URL_PREFIX+"/dept/list",List.class);
    }
}
