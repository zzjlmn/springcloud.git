package cn.com.service.Impl;

import cn.com.dao.deptDao;
import cn.com.service.DeptService;
import cn.com.springcloudPojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private deptDao  deptdao;

    @Override
    public boolean addDept(Dept dept) {
        return deptdao.addDept(dept);
        //return true;
    }

    @Override
    public Dept queryBiId(Long id) {
       return deptdao.queryBiId(id);
       //return new Dept("1111");
    }

    @Override
    public List<Dept> selectAll() {
        return deptdao.selectAll();

       /* List<Dept> list=new ArrayList<>();
        Dept d1=new Dept("22222");
        list.add(d1);
        Dept d2=new Dept("23222");
        list.add(d2);

        return list;
    }*/
    }
}
