package cn.com.service;

import cn.com.springcloudPojo.Dept;

import java.util.List;


public interface DeptService {
    public boolean addDept(Dept dept);

    public Dept queryBiId(Long id);

    public List<Dept> selectAll();
}
