package cn.com.dao;

import cn.com.springcloudPojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface deptDao {
    public boolean addDept(Dept dept);

    public Dept queryBiId(Long id);

    public List<Dept> selectAll();
}
