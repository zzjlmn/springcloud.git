package cn.com.controller;


import cn.com.service.DeptService;
import cn.com.springcloudPojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeptController {

    @Autowired
    private  DeptService deptService;

    @RequestMapping("/dept/add")
    public boolean addDept(Dept dept){
         return deptService.addDept(dept);
    }

    @RequestMapping("/dept/query/{id}")
    public Dept query(@PathVariable("id") Long id){
        return deptService.queryBiId(id);
    }

    @RequestMapping("/dept/list")
    public List<Dept> selectAll(){
        return deptService.selectAll();
    }
}
